var fs = require('fs');
var webdriverio = require('webdriverio');

var dir = './output';

if (!fs.existsSync(dir)){
    fs.mkdirSync(dir);
}


var options = {
    desiredCapabilities: {
        browserName: 'firefox'
    }
};

var path = process.cwd();
var browser = webdriverio
    .remote(options)
    .init();

var projectID = 200344930;

browser.url("file://" + path + "/main.html")
    .element('#projectID')
    .setValue(projectID)
    .element('#processButton').click().pause(10000)
    .element('#output')
    .getValue()
    .then(function(output) {
        console.log('Output is: ' + output);
    
        fs.writeFile(dir+"/"+projectID+".json", output, function(err) {
        if(err) {
            return console.log(err);
        }

            console.log("The file was saved!");
        });  
    })
    .end()
    .catch(function(err) {
        console.log(err);
    });
