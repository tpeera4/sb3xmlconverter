package analysis.utils;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class SB3XMLConverterTest {

	@Test
	public void testConverter() throws Exception {
		String testProjectID = "119615668";
		String projectSrc = new SB3XMLConverter().getSB3ProjectInXML(testProjectID);
		assertTrue(projectSrc.length()>0);
		System.out.println(projectSrc);
	}

}
